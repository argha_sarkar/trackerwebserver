<p class="newdevice">Add new device</p>

<div id="newdevices">

	<form action="newDeviceWork.php" method="post">
      
    	<table class="devices">
    	
    		<tr>
    			<td class="newdevice">Serial number:</td>
    			<td><input type="text" name="serial" maxlength="16" minlength="16"></td>
    		</tr>
    		
    		<tr>
    			<td class="newdevice">MAC address:</td>
    			<td><input type="text" name="mac" maxlength="17" minlength="17"></td>
    		</tr>
    		<tr>
    			<td class="newdevice">Device name:</td>
    			<td><input type="text" name="device_name" maxlength="20" minlength="1"></td>
    		</tr>
    	 
    	</table>
    	
    	<br><br>
    	
    	<div id="newdevicebuttonpadding">
    		<input class='btn' type='submit' name='submit' value='Add New Device'>
    	</div>
    	
    </form>

</div>