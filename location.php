<?php 
	/* Checks if the user is logged in or out. 
	   If the user is logged in, then gets the user ID of the user.
	*/
	session_start(); 
	$loggedIn = true;
	
	if (isset($_SESSION["user_id"]) == true) {
		$loggedIn=true;
		$user_id = $_SESSION["user_id"];
		$name = $_SESSION["name"];
		$email = $_SESSION["email"];
    } else {
		$loggedIn=false;
		header("Location: https://findmyride.co.uk/project");
    }
    // API KEY = AIzaSyBdc4m0AjMZJDUZXifsWhBLxcmK04uklXw	
    require 'keyclasses.php';
    $KC_L = new keyclass_locations();
	$KC_D = new keyclass_devices();
?>

<!DOCTYPE html>
<head>
	<title>Find My Ride</title>
	
	<style type="text/css">
      html, body, #map-canvas { height: 90%; margin: 0; padding: 10px;}
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdc4m0AjMZJDUZXifsWhBLxcmK04uklXw">
    </script>
     
	<?php
		// GETs the device information 
		$device_serial = $_GET["serial"];
		$device_mac = $_GET["mac"];
				
		$loc_long = $KC_L->getLastDeviceLocationLong($device_serial, $device_mac);
		$loc_lat = $KC_L->getLastDeviceLocationLat($device_serial, $device_mac);
		$server_time = $KC_L->getLastDeviceServerTime($device_serial, $device_mac);
		
		// This bit of the code is to verify that the person logged in actually owns the device
		// Sets a boolean indicating if the owner is valid or not
		$validOwner = false;
		// Gets the device id if it exists
		$device_id = $KC_D->deviceExists($device_serial, $device_mac);
		
		if ($device_id > -1) {
			$validOwner = $KC_D->verifyDeviceOwner($user_id, $device_id);
		}
		
		if ($validOwner == true) {
			echo "
			<script type='text/javascript'>
			
			function initialize() {
				var mapOptions = {
       
				center: { lat: $loc_lat, lng: $loc_long },
					zoom: 18
				};
				var map = new google.maps.Map(document.getElementById('map-canvas'),
				mapOptions);
			
				// To add the marker to the map, use the 'map' property
				var marker = new google.maps.Marker({
					position: { lat: $loc_lat, lng: $loc_long },
					map: map,
				
					labelContent: 'hhhhhhhhhhh',
					labelAnchor: new google.maps.Point(22, 0),
					labelClass: 'labels', // the CSS class for the label
					labelStyle: {opacity: 0.75}

				});
			
				var iw1 = new google.maps.InfoWindow({
					content: 'Time: $server_time'			//content: 'Time'
				});
				google.maps.event.addListener(marker, 'click', function (e) { iw1.open(map, this); });

				}
				google.maps.event.addDomListener(window, 'load', initialize);
			</script>";
		}
		
	?>
    
</head>
<body>

	<?php
		include 'header.php';
	
		include 'locationDis.php';
	?>
</body>
</html>