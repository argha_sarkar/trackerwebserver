<?php
/* Checks if the user is logged in or out.
   If the user is logged in, then gets the user ID of the user.
*/
session_start();
$loggedIn = true;

if (isset($_SESSION["user_id"]) == true) {
    $loggedIn=true;
    $user_id = $_SESSION["user_id"];
    $name = $_SESSION["name"];
    $email = $_SESSION["email"];
} else {
    $loggedIn=false;
    header("Location: https://findmyride.co.uk/project");
}

require 'keyclasses.php';

$KC_L = new keyclass_locations();
$KC_D = new keyclass_devices();

// GETs the device information
$device_serial = $_GET["serial"];
$device_mac = $_GET["mac"];

$loc_long = $KC_L->getLastDeviceLocationLong($device_serial, $device_mac);
$loc_lat = $KC_L->getLastDeviceLocationLat($device_serial, $device_mac);
$server_time = $KC_L->getLastDeviceServerTime($device_serial, $device_mac);
?>

<!DOCTYPE html>
<head>
    <title>Find My Ride</title>

    <style type="text/css">
        html, body, #map-canvas { height: 90%; margin: 0; padding: 10px;}
    </style>

    <!-- The Google Map API -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdc4m0AjMZJDUZXifsWhBLxcmK04uklXw"></script>

    <!-- The Pusher API -->
    <script src="https://js.pusher.com/3.0/pusher.min.js"></script>

    <!-- Script for logging Pusher messages -->
    <script type="text/javascript">
        // Constants values
        const PUSHER_KEY = '096c10cc539d5d015053';

        const RED_POLYLINE = '#FF0000';
        const AMBER_POLYLINE = '#FF6600';
        const YELLOW_POLYLINE = '#FFCC00';
        const GREEN_POLYLINE = '#00FF00';

        const SPEED_STOPPED = 0.99;
        const SPEED_SLOW = 4.99;
        const SPEED_MEDIUM = 14.99;
        const SPEED_FAST = 29.99;

        // Global variables for storing data between initialise and the updateMap function()
        var globalMap = null;
        var globalMapOptions = null;
        var globalMarker = null;
        var globalMarkerInfoWindow1 = null;

        /**
         * Global variables to create the polyline between the previous location and the current location.
         * Will contain an array containing the longitude and latitude. Initialised as empty.
         * EG: previousLocation = { lat: 0, lng: 0};
         */
        var previousLocation  = [];
        var currentLocation = [];

        // Enable pusher logging - don't include this in production
        Pusher.log = function(message) {
            if (window.console && window.console.log) {
               // window.console.log(message);
            }
        };

        var pusher = new Pusher(PUSHER_KEY, {
            cluster: 'eu',
            encrypted: true
        });

        var channel = pusher.subscribe('test_channel');
        channel.bind('location_update', function(data) {
            // Function goes here to update the map
            updateMap(data);
        });
        function updateMap(data) {
            //FIXME: This only gets called during the initialisation. Need to work on this and update very single time the map is updated.
            var serverTime = <?php echo "\"" . $KC_L->getLastDeviceServerTime($device_serial, $device_mac) . "\";"; ?>

            // Only update
            if (data.update = true) {
                // Logging the info received.
                /*console.log([
                    "lat: " + data.lat,
                    "lng: " + data.lng,
                    "serverTime: " + serverTime
                ]);*/

                // Updating the current location and drawing the polyline
                drawPolylineTracking(data);

                // Removing the old marker
                globalMarker.setMap(null);
                globalMarker = null;
                // Creating the new marker
                globalMarker = new google.maps.Marker({
                    position: { lat: parseFloat(data.lat), lng: parseFloat(data.lng) },
                    map: globalMap,
                    labelContent: 'hhhhhhhhhhh',
                    labelAnchor: new google.maps.Point(22, 0),
                    labelClass: 'labels', // the CSS class for the label
                    labelStyle: {opacity: 0.75}

                });
                globalMarker.setMap(globalMap);

                globalMap.setCenter(globalMarker.getPosition());

                // Setting the marker's info
                globalMarkerInfoWindow1 = new google.maps.InfoWindow({
                    content: "Time: " + serverTime + "  Speed: " + data.speed
                });
                globalMarkerInfoWindow1.open(globalMarker.map, globalMarker);
            }

        }

        /**
         * Function to draw polyline depending on speed between the previous location and the current location
         */
        function drawPolylineTracking(data) {

            if (currentLocation.lat != 0) {
                previousLocation = { lat: currentLocation.lat, lng: currentLocation.lng };
            }
            currentLocation = { lat: parseFloat(data.lat), lng: parseFloat(data.lng) };

            // Deciding ont he colour of the POLYLINE dependent on speed
            var polylineColour = getColourFromSpeed(data.speed);

            if (currentLocation.lat != 0 && previousLocation.lat != 0) {
                console.log("Trying to work");
                var vehiclePath = new google.maps.Polyline({
                    path: [previousLocation, currentLocation],
                    geodesic: true,
                    strokeColor: polylineColour,
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                vehiclePath.setMap(globalMap);
            }
        }

        // Returns the colour of the polyline depending on the vehicle speed
        function getColourFromSpeed(currentSpeed) {
            if (currentSpeed < SPEED_STOPPED) {
                return RED_POLYLINE;
            }
            if (currentSpeed < SPEED_SLOW) {
                return AMBER_POLYLINE;
            }
            if (currentSpeed < SPEED_MEDIUM) {
                return YELLOW_POLYLINE;
            }
            return GREEN_POLYLINE;
        }
    </script>

    <?php


    // This bit of the code is to verify that the person logged in actually owns the device
    // Sets a boolean indicating if the owner is valid or not
    $validOwner = false;
    // Gets the device id if it exists
    $device_id = $KC_D->deviceExists($device_serial, $device_mac);

    if ($device_id > -1) {
        $validOwner = $KC_D->verifyDeviceOwner($user_id, $device_id);
    }

    if ($validOwner == true) {
        echo "
			<script type='text/javascript'>

			function initialize() {
				var mapOptions = {

				    center: { lat: $loc_lat, lng: $loc_long },
					zoom: 18
				};
				var map = new google.maps.Map(document.getElementById('map-canvas'),
				mapOptions);

				// To add the marker to the map, use the 'map' property
				var marker = new google.maps.Marker({
					position: { lat: $loc_lat, lng: $loc_long },
					map: map,

					labelContent: 'hhhhhhhhhhh',
					labelAnchor: new google.maps.Point(22, 0),
					labelClass: 'labels', // the CSS class for the label
					labelStyle: {opacity: 0.75}

				});

				var iw1 = new google.maps.InfoWindow({
					content: 'Time: $server_time'			//content: 'Time'
				});
				google.maps.event.addListener(marker, 'click', function (e) { iw1.open(map, this); });

                globalMapOptions = mapOptions;
                globalMap = map;
                globalMarker = marker;
                globalMarkerInfoWindow1 = iw1;
            }
				google.maps.event.addDomListener(window, 'load', initialize);
			</script>";
    }

    ?>

</head>
<body>

<?php
include 'header.php';

include 'locationDis.php';
?>
</body>
</html>