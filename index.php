<?php 
	/* Checks if the user is logged in or out. 
	   If the user is logged in, then gets the user ID of the user.
	*/
	session_start(); 
	$loggedIn = true;
	
	if (isset($_SESSION["user_id"]) == true) {
		$loggedIn=true;
		$user_id = $_SESSION["user_id"];
		$name = $_SESSION["name"];
		$email = $_SESSION["email"];
    } else {
		$loggedIn=false;
    }
	
?>
<!DOCTYPE html>
<head>
	<title>Find My Ride</title>
</head>
<body>

	<script src="jquery-2.2.2.min.js"></script>
	<script type="text/javascript">
	
		function hideCookie() {
		 		 console.log("click");
				 $("#cookieBar").hide(1000);
		}
		
	</script>
	
	<?php
		include 'header.php';
		
		if (isset($_SESSION["user_id"]) == true) {
			// The user is logged in.
			echo "<p class = 'indexuserloginmessage'>";
			echo "Welcome $name!<br> </p>";
		} else {
			// The user is logged out. 
			echo "<p class = 'indexuserloginmessage'>Please login or register to Find your car!</p>";
		}
	?>
	
	<p class = "generalText"> 
		<img src="resources/images/device1.jpg" alt="Tracking device" width="75%" height="75%">
		<br><br>
		
		Each year over 370,000 cars are stolen in the UK alone costing the economy billions of pounds. 
		To combat these these problem, all new cars are fitted with sophisticated steering locks and 
		engine immobilisers. To get around this, thieves part of organised criminal gangs often tow the
		vehicle and strip it for its parts or export them under a new identity. As a result, most 
		premium car manufacturers such as Mercedes are now including GPS tracking system called "Tracker
		Plus" as standard in new cars. These premium cars are the only cars on the road and are certainly
		not the only ones being stolen every year. My project involves creating a system to track cars 
		if they are stolen and track the owner’s driving. 
		
		<br><br>
		This project involves creating a hardware device along with a mobile phone app (Android) and a 
		web app which would allow vehicle owners to track	their cars in the unfortunate event of it 
		getting stolen or generally to observe their driving over a period of time. The tracking system 
		would allow the owner to track the routes taken by all the drivers of the vehicle and how much 
		time each trip required. This feature will work in a similar manner to fitness tracking apps but
		for cars. There could also be an option for the	refueling times and amounts to be entered which 
		would help measure the efficiency of each drive.
	
	</p>

	<div id="cookieBar">
		We use cookies to ensure that you have the best possible experience.
		If you're happy with that, just carry on as normal - otherwise you can 
		change your browser's settings at any time. 
		<button onclick="hideCookie()" id="cookieBarButton">Close</button>
	</div>
</body>

</html>