<?php

require 'Pusher.php';
require 'PusherCredentials.php';

class PusherHelper
{
    /**
     * @param array $notification -- Associative array
     *                               Must contain 'group', 'event', 'name' and 'url'  keys
     * @return bool
     */
    public function sendPusherNotification(array $notification) : bool {
        $options = array(
            'cluster' => 'eu',
            'encrypted' => false
        );
        $pusherCredentials = new PusherCredentials();
        $pusher = new Pusher(

            $pusherCredentials->getKey(),
            $pusherCredentials->getSecret(),
            $pusherCredentials->getAppId(),

            $options
        );

        $status = $pusher->trigger($notification['device'], $notification['event'], $notification);
        return $status;
    }

}