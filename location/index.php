<?php 
	/* Checks if the user is logged in or out. 
	   If the user is logged in, then gets the user ID of the user.
	*/
	session_start(); 
	$loggedIn = true;
	
	if (isset($_SESSION["user_id"]) == true) {
		$loggedIn=true;
		$user_id = $_SESSION["user_id"];
		$name = $_SESSION["name"];
		$email = $_SESSION["email"];
    } else {
		$loggedIn=false;
		header("Location: https://findmyride.co.uk/project");
    }
	
?>

<!DOCTYPE html>
<head>
	<title>Find My Ride</title>
</head>
<body>

	<?php
		include '../header.php';
	?>