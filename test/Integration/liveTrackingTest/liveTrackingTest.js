var request = require("request");
var fs = require('fs');

// The interval between the different pings are sent to the server
const PING_INTERVAL = 800;
const FILE_NAME = "data-1.05.16.csv";

/**
 * Array containing array of location data: Each element in this array contains another element
 * locationDataArray[0] = [locationId, deviceTime, serverTime, deviceIPAddr, speedInKnots, latitude, longitude];
 * locationDataArray[0] = [26586, "2016-04-17 15:26:12", "2016-04-17 16:26:05", "82.132.232.35", 0.108, 51.571643333, -0.375943333];
 */
var locationDataArray = [];
// Global loop counter variable for locationDataArray
var i = 0;


// Read the text file containing the location data
var loadTestData = function () {
    var tempDataArray = fs.readFileSync(FILE_NAME).toString().split("\n");
    for(i in tempDataArray) {
        locationDataArray[i] = tempDataArray[i].split(',');
    }
}

/**
 * Runs the simulation. Sends the POST request back to the server with the location every one second
 */
function pingServer() {
    setTimeout(function () {

        var options = {
            method: 'POST',
            url: 'https://findmyride.co.uk/project/device/recLocation.php',
            headers: {
                'postman-token': '2992336a-2c61-4474-8900-ad42d9a35caf',
                'cache-control': 'no-cache',
                'content-type': 'multipart/form-data; boundary=---011000010111000001101001'
            },
            formData: {
                time_device: locationDataArray[0][1],
                speed: locationDataArray[i][4],
                lat: locationDataArray[i][5],
                long: locationDataArray[i][6],
                serial: '1111111111111111',
                mac: '22:22:22:22:22:22'
            }

        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            console.log("i: " + i + "   Body: " + body);
        });

        i++;

        if (i < locationDataArray.length) {
            pingServer();
        }
    },  PING_INTERVAL);

}

function init() {
    // Loading the test data
    loadTestData();

    i = 0;
    pingServer();
}

init();