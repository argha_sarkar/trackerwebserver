<?php 
class Database {
	
	function exec($query) {
		$db = $this->getConnection();
		$db->query($query);
		$db->close();
	}
	
	function run_query($query) {
		$db = $this->getConnection();
		if ($db == "error") {
			return "error";
		}
		$result = $db->query($query);
		$db->close();
		return $result;
		
	}
	
	function querySingle($query) {
		$db = $this->getConnection();
		$result = $db->querySingle($query,true);
		$db->close();
		return $result;
	}
	
	function prepare($query) {
		$db = $this->getConnection();
		$db->close();
		return $db->prepare($query);
	}
	
	function escapeString($string) {
		$db = $this->getConnection();
		$db->close();
		return $db->escapeString($string);
	}
	
	function getConnection() {
		
		$servername = "localhost";
		$username = "root";
		$password = "pooface";
		$dbName = "argha_project";
		
		$conn = new mysqli($servername, $username, $password, $dbName);
		
		if (!$conn) {
			return "error";
			//die("Connection failed: " . mysqli_connect_error());
		}
		
		return $conn;
	}
}
?>