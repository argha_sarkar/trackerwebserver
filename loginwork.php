<?php
	session_start();
	/*
		Importing database and keyclasses PHP file.
		Keyclasses has already imported the database file.
	*/
	require 'keyclasses.php';
	
	// Declaring the key class. It contains useful functions.
	$KC = new keyclass();
	//Declaring the key class for log in.
	$KC_L = new keyclass_login();
	// Declaring the database class.
	$DB = new Database;
	
	/*
		Checks if the user is logged in or logged out.
	*/
	if (isset($_SESSION["user_id"]) == true | isset($_SESSION["email"] ) == true) {
		// Logs out the user because the user is logged in
		session_unset();
		session_destroy();
		header("Location: ./login.php");
	} else {
		// Attempts to login the user
		$email = $_POST["email"];
		$password = $_POST["password"];
		
		$login_status = $KC_L->authenticateUser($email, $password);
		
		// If log in is successful then sets session cookies.
		if ($login_status == True) {
			$user_id = $KC_L->getIdFromEmail($email);
			$name = $KC_L->getName($user_id);
			$_SESSION["user_id"] = $user_id;
			$_SESSION["email"] = $email;
			$_SESSION["name"] = $name;
			header("Location: ./");
		} else {
			$input_errors = "Incorrect email or password.";
			header("Location: ./login.php?errors=$input_errors");
		}
		
	}

?>