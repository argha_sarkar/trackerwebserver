<?php

// Normal - Vulnerable database class
require 'database.php';
// Prepared database class
require 'databaseP.php';

class keyclass {

	/*
		Contains a lot of important functions about the registration
	*/
	
	function sanitiseString($input) {
		/*
			Removes certain special characters which might cause security issues
		*/
		$input = str_replace(" ","",$input);
		$input = str_replace("'", '', $input);
		$input = str_replace(";", '', $input);
		$input = str_replace(",", '', $input);
		$input = str_replace(")", '', $input);
		$input = str_replace("(", '', $input);
		//$input = str_replace("-", '', $input);
		//$input = str_replace(".", '', $input);
		return $input;
	}
	
	function checkEmailInDb($input_email) {
		/*
			Checks if a given email already exists in the database.
		*/
		
		// Starts a new database class.
		$DB = new Database();
	
		$input_email = $this->sanitiseString($input_email);
		
		$sql1 = "SELECT `user_email` FROM `tbl_user` WHERE `user_email`='$input_email';";
		$results = $DB->run_query($sql1);
		
		// Number of results returned. If > 0 then email exists.
		$resultsReturned = $results->num_rows;
		
		if ($resultsReturned  > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function insertUserInDbP($fname, $sname, $email, $address, $postcode, $pass) {
		/*  ***PARAMETERISED***
			Inserts the user data into the database.
			Creates a new user entry in the database.
		*/
		// Initialises new database class.
		$DB = new DatabaseP();
		// Getting a database connection.
		$dbConn = $DB->getConnection();
		
		// Base SQL statement
		$sqlStmt = "INSERT INTO `tbl_user`(`user_fname`, `user_sname`, `user_email`, `user_address`, `user_postcode`, `user_salt`, `user_hash`) VALUES (?,?,?,?,?,?,?);";
		
		// Generating a salt - used for hashing. No two same passwords will hash to the same value
		$salt = $this->generateSalt();
		$hashPass = $this->generatePassHash($salt, $pass);
		
		// Prepares the SQL statement to enable parameteriasation.
		if ($prepStmt = $dbConn->prepare($sqlStmt)) {
			// Preparation has been successful.
								
			if ($prepStmt->bind_param("sssssss", $fname, $sname, $email, $address, $postcode, $salt, $hashPass)) {
				// Successful bind.
				
				if ($prepStmt->execute()) {
					// Closing the prepared statement
					$prepStmt->close();
					// Closing the database connection
					$dbConn->close();
					// Returning true
					return true;
				} else {
					echo "Execution failed.<br>";
				}
				
			} else {
				echo "Binding parameters failed.<br>";
			}
			
		} else {
			echo "Prepare statement failed.";
		}
		
		//Returning false	
		return false;
	}
	
	function generatePassHash($salt, $pass) {
		/*
			Takes in the raw password text and returns a hashed value.
			The hashed value stored in the database will padded using the salt.
		*/
		$pass = $salt . "-" . $pass;
		$pass = hash('sha256', $pass);
		return $pass;
	}
	
	function generateSalt() {
		/*
			Generates a 10 digit long random number
		*/
		$salt = "" . rand(1000000000, 9999999999);
		return $salt;
	}
	
	function getUtcTime() {
		/*
			Returns UTC time same as the GPS format.
		*/
		$time = time();
		$check = $time+date("Z",$time);
		$date = strftime("%B %d, %Y @ %H:%M:%S UTC", $check);
		$date = gmdate("Y-m-d\TH:i:s\.00Z");
		$date .= "Z";
		
		return $date;
	}
	
}

class keyclass_login {

	/*
		Contains important functions needed for the login to work
	*/
	
	function getIdFromEmail($email) {
		/*
			Gets the user id for the user given their email address
		*/
		// Sanitising the string
		$KC = new keyclass;
		$DB = new Database();
		$email = $KC->sanitiseString($email);
		
		$sql1 = "SELECT * FROM `tbl_user` WHERE `user_email`='$email';";
		$result = $DB->run_query($sql1);
		
		// Initialising user id as -1
		$user_id = -1;
		
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$user_id = $row["user_id"];
			}
		}
		
		return $user_id;
	}
	
	function getSalt($user_id) {
		/*
			Gets the password salt from the user id.
		*/
		$salt = -1;
		$sql1 = "SELECT * FROM `tbl_user` WHERE `user_id` = '$user_id';";
		
		$DB = new Database();
		$result = $DB->run_query($sql1);
		
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$salt = $row["user_salt"];
			}
		}
		return $salt;
	}
	
	function getName($user_id) {
		/*
			Gets the password salt from the user id.
		*/
		$name = "-1";
		$sql1 = "SELECT * FROM `tbl_user` WHERE `user_id` = '$user_id';";
		
		$DB = new Database();
		$result = $DB->run_query($sql1);
		
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$name = $row["user_fname"] . " " . $row["user_sname"];
			}
		}
		return $name;
	}
	
	function getHash($user_id) {
		/*
			Gets the hashed password from the user id.
		*/
		$hash = "-1";
		$sql1 = "SELECT * FROM `tbl_user` WHERE `user_id` = '$user_id';";
		
		$DB = new Database();
		$result = $DB->run_query($sql1);
		
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$hash = $row["user_hash"];
			}
		}
		return $hash;
	}
	
	function authenticateUser($email, $password) {
		/*
			This is an important function. Checks if the user input details are correct.
			Returns a boolean value indicating if the user can log in or not.
			True - Correct email and password combo received.
			False - Display error message. Incorrect user email and password combo.
		*/
		
		$KC = new keyclass();
		
		$user_id = $this->getIdFromEmail($email);
		$user_salt = $this->getSalt($user_id);
		$user_hash = $this->getHash($user_id);
		$input_hash = $KC->generatePassHash($user_salt, $password);
		
		if ($input_hash == $user_hash) {
			return true;
		}		
		return false;
	}

}

class keyclass_devices {
	/*
		This class contains functions relating to the devices and devices owned by users.
	*/
	
	function getDeviceIdBySerial($serial_number) {
		/*
			Returns the device id for a given the serial number
		*/
		$device_id = -1;
		
		$DB = new Database();
		$sql1 = "SELECT * FROM `tbl_device` WHERE `device_serial` = '$serial_number';";
		$result = $DB->run_query($sql1);
		
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$device_id = $row["device_id"];
			}
		}
		
		return $device_id;
	}
	
	function getDevices($user_id) {
		/*
			Returns the device id(s) which are owned by the given user id.
		*/
		$DB = new Database();
		$sql1 = "SELECT * FROM `tbl_device` WHERE `device_id` IN (SELECT `du_device_id` FROM `tbl_device_user` WHERE `du_user_id` = '$user_id');";
		$result = $DB->run_query($sql1);
		
		return $result;
	}
	
	function deleteDeviceUserRef($device_id) {
		/*
			Used by owners for deleting devices from their accounts.
			Deletes the device to user reference from the device-user table.
			Deletes the record for the given user_id.
		*/
		$DB = new Database();
		$sql1 = "DELETE FROM `tbl_device_user` WHERE `du_device_id`='$device_id';";
		$result = $DB->run_query($sql1);
		
		return $result;
	}
	
	function verifyDeviceOwner($user_id, $device_id) {
		/*
			Verifies if the given user owns the given device.
			This is done by looking up on the device-user table.
			Returns true if the user is the owner otherwise returns false.
		*/
		$DB = new Database();
		$sql1 = "SELECT * FROM `tbl_device_user` WHERE `du_user_id`='$user_id' AND `du_device_id` = '$device_id';";
		$result = $DB->run_query($sql1);
		
		if ($result->num_rows > 0) {
			return true;
		}
		return false;
	}
	
	function deviceExists($device_serial, $device_mac) {
		/*
			Verifies if the device with the given serial number and mac address exists
			in the system database. 
			Returns device_id if the device exists. Otherwise returns -1.
		*/
		$DB = new Database();
		$sql1 = "SELECT * FROM `tbl_device` WHERE `device_serial` = '$device_serial' AND `device_mac` = '$device_mac';";
		$result = $DB->run_query($sql1);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$device_id = $row["device_id"];
			}
			return $device_id;
		} 
		
		return -1;
	}
	
	function deviceOwnStatus($device_id) {
		/*
			Checks if a registered device already has an owner or not.
			Returns user_id if already owned. Otherwise returns -1.
		*/
		$DB = new Database();
		$user_id = -1;
		$sql1 = "SELECT * FROM `tbl_device_user` WHERE `du_device_id` ='$device_id'";
		$result = $DB->run_query($sql1);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$user_id = $row["du_user_id"];
			}
		}
		
		return $user_id;
	}
	
	function addDeviceOwner($user_id, $device_id) {
		/*	
			Creates a new relation between an user and a device in the device-user table.
		*/
		$DB = new Database();
		$sql1 = "INSERT INTO `tbl_device_user`(`du_user_id`, `du_device_id`) VALUES ('$user_id','$device_id'); ";
		$result = $DB->run_query($sql1);
		
		return $result;
	}
	
	function addDeviceOwnerP($user_id, $device_id) {
		/*	***PARAMETERISED***
			Creates a new relation between an user and a device in the device-user table.
		*/
		$DB = new Database();
		$sql1 = "INSERT INTO `tbl_device_user`(`du_user_id`, `du_device_id`) VALUES ('$user_id','$device_id'); ";
		$result = $DB->run_query($sql1);
		
		return $result;
	}
	
	function addDeviceName($device_id, $device_name) {
		/*
			Updates the database record for the given device id. 
			The device_name field is filled in according to the user's decision.
		*/
		$DB = new Database(); 
		$sql1 = "UPDATE `tbl_device` SET `device_name`= '$device_name' WHERE `device_id` = '$device_id';";
		$result = $DB->run_query($sql1);
		
		return $result;
	}
	
	function addNewDevice($serial, $mac) {
		/*
			Adds a new device to the database. This will be used by the back-end to add
			a new device to the database. If the device to be added already exists in the database, 
			then the last reported time is uploaded. This program is run every single time on boot up.
		*/
		$DB = new Database();
		$KC = new keyclass();
		
		$time = $KC->getUtcTime();
		$mac = $KC->sanitiseString($mac);
		$serial = $KC->sanitiseString($serial);
		
		// Checks if the device exists on the database
		$device_id = $this->deviceExists($serial, $mac);
		if ($device_id < 0) {
			// Device does not exist.
			// If the device is not registered on the network, then add it to the database. 				
			$sql1 = "INSERT INTO `tbl_device`(`device_serial`, `device_mac`, `device_time_added`) VALUES ('$serial','$mac','$time');";
			$result = $DB->run_query($sql1);
		} else {
			// The device has already been registered on the network.
			$curTime = $KC->getUtcTime();
			$sql2 = "UPDATE `tbl_device` SET`device_time_added`= '$curTime' WHERE `device_serial` = '$serial' AND `device_mac` = '$mac';";
			$result = $DB->run_query($sql2);
		}
	
		return $result;
	}
	
	function addBrandNewDevice($serial, $mac) {
		/*
			Adds a new device to the database every single time the device is switched on. 
			This is similar to the "addNewDevice" function except if a device already exists
			in the database, the old record is not updated. A new device is added regardless.
		*/
		$DB = new Database();
		$KC = new keyclass();
		
		$time = $KC->getUtcTime();
		$mac = $KC->sanitiseString($mac);
		$serial = $KC->sanitiseString($serial);
		
		$sql1 = "INSERT INTO `tbl_device`(`device_serial`, `device_mac`, `device_time_added`) VALUES ('$serial','$mac','$time');";
		$result = $DB->run_query($sql1);
		
		return $result;
	}
	
	function receiveLocation($location_time_device, $location_speed, $location_lat, $location_long, $device_serial, $device_mac) {
		/*
			This function takes the parameters sent by the device to the server and adds the details to the database.
			Out of all the data that is added to the database, some is received by GET/POST requests and the others 
			are acquired by the server using PHP functions and arrays.
			There will be two INSERT statements:
				- The first INSERT statement will be for entering the location data into the database. 
				- The location ID must be returned using a SELECT statement. The SELECT statement will return the last location_iHTTP_CLIENT_IP'd for the given lat, long and speed.
				- After the location_id is retrieved, the device_id must be retrieved for the given serial number and the mac address.
				- The second INSERT statement will be entered in the device_location table. The location_id and the device_id will be entered. 
		*/
	
		$DB = new Database();
		$KC = new keyclass();
	
		$device_ip = $_SERVER['REMOTE_ADDR'];
	
		$sql1A = "INSERT INTO `tbl_location`(`location_time_device`, `location_ip`, `location_speed`, `location_lat`, `location_long`) VALUES ";
		$sql1B = "('$location_time_device', '$device_ip', '$location_speed', '$location_lat', '$location_long');";
		
		$sql1 = $sql1A . $sql1B;
		$result1 = $DB->run_query($sql1);
		
		$location_id = $this->getLocationId($location_speed, $location_lat, $location_long); 
		$device_id = $this->deviceExists($device_serial, $device_mac);
		
		$answer = $this->joinLocationDevice($location_id, $device_id);
		
		return $answer;
		
	}
	
	function getLocationId($location_speed, $location_lat, $location_long) {
		/*
			Returns the location id. Input parameters: speed, latitude, longitude and time. 
		*/
		$DB = new Database();	
		
		$sql2 = "SELECT * FROM `tbl_location` WHERE `location_speed` = '$location_speed' AND `location_lat` = '$location_lat' AND `location_long` = '$location_long' ORDER BY `location_id` DESC LIMIT 1;";
		$result2 = $DB->run_query($sql2);
		
		$location_id = 0;
		
		if ($result2->num_rows > 0) {
			while ($row2 = $result2->fetch_assoc()) {
				$location_id = $row2["location_id"];
			}
		}	
		
		return $location_id;
	}
	
	function joinLocationDevice($locationId, $deviceId) {
		/*
			Given a device id and a location id, this function creates a record in the 
			link table. This joins the device to a given location in the location table. 
			The device is then linked to an user. 
		*/
		$DB = new Database();
		$sql1 = "INSERT INTO `tbl_device_location`(`dl_location_id`, `dl_device_id`) VALUES ('$locationId', '$deviceId');";
		$result1 = $DB->run_query($sql1);
		
		return $sql1;
	}
	
}

class keyclass_locations {

	function getAllDeviceLocations($serial, $mac, $startId, $limit) {
		/*
			Returns all the locations for a given serial and mac pair.
		*/
		$DB = new Database();
		
		$sql1 = "SELECT * FROM `tbl_location` WHERE `location_id` IN (SELECT `dl_location_id` FROM `tbl_device_location` WHERE `dl_device_id` = (SELECT `device_id` FROM `tbl_device` WHERE `device_serial` = '$serial' AND `device_mac` = '$mac')) LIMIT $startId, $limit;";
		$result1 = $DB->run_query($sql1);
				
		return $result1;
	}

	function getLastDeviceLocation($serial, $mac) {
		/*
			This class gets the last known location details for a given deivce. 
			Device is identified by serial number and mac address.
			The entire associative row is returned. 
		*/
		$DB = new Database();
		
		// SELECT * FROM `tbl_location` WHERE `location_id` = (SELECT `dl_location_id` FROM `tbl_device_location` WHERE `dl_device_id` = (SELECT `device_id` FROM `tbl_device` WHERE `device_serial` = '00000000b19869e7' AND `device_mac` = 'b8:27:eb:98:69:e7' ORDER BY `device_id` DESC LIMIT 1) ORDER BY `dl_location_id` DESC LIMIT 1);
		$sql1 = "SELECT * FROM `tbl_location` WHERE `location_id` = (SELECT `dl_location_id` FROM `tbl_device_location` WHERE `dl_device_id` = (SELECT `device_id` FROM `tbl_device` WHERE `device_serial` = '$serial' AND `device_mac` = '$mac' ORDER BY `device_id` DESC LIMIT 1) ORDER BY `dl_id` DESC LIMIT 1) ORDER BY `location_id` DESC LIMIT 1;";
		$result1 = $DB->run_query($sql1);
		
		$dataRow = null;
		
		if ($result1->num_rows > 0) {
			while ($row1 = $result1->fetch_assoc()) {
				//$dataRow = $row1["location_lat"];
				$dataRow = $row1;
			}
		}
		
		return $dataRow; 
	}
	
	function getLastDeviceLocationLong($serial, $mac) {
		/*
			Returns the last device longitude for given serial number and mac address.
			Uses "getLastDeviceLocation" to get the last row. Then just extracts data.
		*/
		$row = $this->getLastDeviceLocation($serial, $mac);
		$longitude = $row["location_long"];
		return $longitude;
	}
	
		function getLastDeviceLocationLat($serial, $mac) {
		/*
			Returns the last device latitude for given serial number and mac address.
			Uses "getLastDeviceLocation" to get the last row. Then just extracts data.
		*/
		$row = $this->getLastDeviceLocation($serial, $mac);
		$latitude = $row["location_lat"];
		
		return $latitude;
	}
	
	function getLastDeviceCoordinate($serial, $mac) {
		/*
			Returns the last longitude and latidue. Uses the previous two functions
		*/
		$long = $this->getLastDeviceLocationLong($serial, $mac);
		$lat = $this->getLastDeviceLocationLat($serial, $mac);
		
		return "$lat, $long";
	}
	
	function getLastDeviceServerTime($serial, $mac) {
		/*
			Returns the last known location reported server time for the given device.
		*/
		$row = $this->getLastDeviceLocation($serial, $mac);
		$server_time = $row["location_time_server"];
		
		return $server_time;
	}
	
	function getLocationsCount($serial, $mac, $limit) {
		/*	
			Returns the number of locations for a certain device
		*/
		$DB = new Database();
		
		$sql1 = "SELECT COUNT(*) FROM `tbl_location` WHERE `location_id` IN (SELECT `dl_location_id` FROM `tbl_device_location` WHERE `dl_device_id` = (SELECT `device_id` FROM `tbl_device` WHERE `device_serial` = '$serial' AND `device_mac` = '$mac'));";
		$result1 = $DB->run_query($sql1);
		$rowCount = 0;
		
		if ($result1->num_rows > 0) {
			while ($row = $result1->fetch_assoc()) {
				$rowCount = $row["COUNT(*)"];
			}
		}
		
		$rowCountVal = intval($rowCount);
		$rowCountVal = floor($rowCountVal / $limit);
		$rowCountVal++;
		
		return $rowCountVal;
	}
	
	function getLastLocation($deviceId) {
		/*
			Returns the location details based on the given device id
		*/
		$DB = new Database;
		
		$sql1 = "SELECT * FROM `tbl_location` WHERE `location_id` = (SELECT `dl_location_id` FROM `tbl_device_location` WHERE `dl_device_id` = $deviceId ORDER BY `dl_location_id` DESC LIMIT 1);";
		$result1 = $DB->run_query($sql1);
		//return $sql1;
		return $result1;		
	}

}

class keyclass_test {

	function testLatency1() {
		/*
			Tests the latency of data transmission throughout the database
		*/
		$DB = new Database();
		
		// Finds the number of records in the database
		$sql1 = "SELECT COUNT(*) FROM `tbl_location`;";
		$result1 = $DB->run_query($sql1);
		$numRecs = -1;
		
		if ($result1->num_rows > 0) {
			while ($row1 = $result1->fetch_assoc()) {
				$numRecs = $row1["COUNT(*)"];
			}
		}	
		
		return $numRecs;
	}
	
	function timeDiff($time1, $time2) {
		// Find the difference of two dates in seconds
		$strTime1 = strtotime($time1);
		$strTime2 = strtotime($time2);
		$timeDifference = $time2 - $time1;
		
		return $timeDifference;
	}
	
	function returnAllTimes() {
		// Returns all the device and server time pairs in the database
		$DB = new Database();
		
		$sql1 = "SELECT `location_time_device`, `location_time_server`FROM `tbl_location`;";
		$result1 = $DB->run_query($sql1);
	
		return $result1;
	}
}

?>