<?php
	require '../keyclasses.php';
	/* 
		This accepts an username and a password
		Returns a value indicating it was a successful log in. Otherwise the app does not accept it.
	*/
	
	// Declaring the key class. It contains useful functions.
	$KC = new keyclass();
	//Declaring the key class for log in.
	$KC_L = new keyclass_login();
	// Declaring the database class.
	$DB = new Database;
	
	
	//Gets the username and password
	$email = $_POST["email"];
	$password = $_POST["pass"];
	
	$loginStatus = $KC_L->authenticateUser($email, $password);
	if ($loginStatus) {
		// User has logged in successfully
		echo "in";
		$user_id = $KC_L->getIdFromEmail($email);
		$name = $KC_L->getName($user_id);
		$_SESSION["user_id"] = $user_id;
		$_SESSION["email"] = $email;
		$_SESSION["name"] = $name;
		//echo "<br>user id: $user_id";
		//echo "<br>Name: $name";
		displayDevices($user_id);
	} else {
		// Invalid username and / or password
		echo "out";
	}
	
	function displayDevices($user_id) {
		/*
			Will display the devices and their last known locations
			1) in / out --> if its not in, then user can't log in. Rest of the lines will only show if the user has logged in
			^ is called already before. This function only deals with line #2+
			2) deviceId, deviceName, lastKnownTime, lastLat, lastLong --> will be repeated for each device owned by the user
		*/
		echo ";\n";
		// Declaring the keyclass for devices.
		$KC_D = new keyclass_devices;
		// Declaring the keyclass for locations.
		$KC_L = new keyclass_locations;
		
		// Gets result for the devices for given user id
		$result1 = $KC_D->getDevices($user_id);
		
		if ($result1->num_rows > 0) {
			while ($row1 = $result1->fetch_assoc()) {
				$deviceId = $row1["device_id"];
				$deviceName = $row1["device_name"];
				// Creating the string
				$deviceString = $deviceId;
				$deviceString .= "," . $deviceName;
				
				// Getting the details about the last location information about this particular device
				$locationResults = $KC_L->getLastLocation($deviceId);
				
				if ($locationResults->num_rows > 0) {
					while ($locationRow = $locationResults->fetch_assoc()) {
						// Getting the required location fields
						$locationId = $locationRow["location_id"];
						$locationTime = $locationRow["location_time_server"];
						$locationSpeed = $locationRow["location_speed"];
						$locationLat = $locationRow["location_lat"];
						$locationLong = $locationRow["location_long"];
						
						$deviceString .= ",$locationId";
						$deviceString .= ",$locationTime";
						$deviceString .= ",$locationSpeed";
						$deviceString .= ",$locationLat";
						$deviceString .= ",$locationLong";
						
					}
				}	
								
				echo $deviceString . ";\n";
			}
		} else {
			echo "none;";
		}
		
		return null;
	}
	
?>